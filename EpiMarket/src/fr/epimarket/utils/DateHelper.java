package fr.epimarket.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper 
{
	public static DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	
	public static Date getDate(String strDate)
	{
		try {return (Date) formatter.parse(strDate);}
		catch (Exception e) {System.out.println("DateHelper: geDate: " + e.toString());
			return null;}
	}
	
	public static String getStringDate(Date dateOrderStarted)
	{
		try {return formatter.format(dateOrderStarted);}
		catch (Exception e) {System.out.println("DateHelper: getStringDate: " + e.toString());
			return null;}		
	}
}
