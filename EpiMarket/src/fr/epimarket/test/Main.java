package fr.epimarket.test;

import java.util.GregorianCalendar;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import fr.epimarket.model.AbstractProduct;
import fr.epimarket.model.Address;
import fr.epimarket.model.Category;
import fr.epimarket.model.Client;
import fr.epimarket.model.Dvd;
import fr.epimarket.model.Game;
import fr.epimarket.model.Order;
import fr.epimarket.model.OrderLine;
import fr.epimarket.dao.AddressDAO;
import fr.epimarket.dao.ClientDAO;
import fr.epimarket.dao.DvdDAO;
import fr.epimarket.dao.GameDAO;
import fr.epimarket.dao.OrderDAO;
import fr.epimarket.dao.OrderLineDAO;
import fr.epimarket.exception.AddressException;
import fr.epimarket.exception.ClientException;
import fr.epimarket.exception.OrderException;
import fr.epimarket.exception.OrderLineException;
import fr.epimarket.exception.ProductException;

public class Main
{
	public static void main(String argv[]) throws ProductException, OrderException, OrderLineException
	{
		MysqlDataSource ds = new MysqlDataSource();
		ds.setUser("root");
		ds.setPassword("");
		ds.setServerName("localhost");
		ds.setDatabaseName("test");
		try {
			/*Address addr = addrDAO.getAddressById(1);
			System.out.print(addr.toString());
			addr.setCity("PARIS");
			addr.setCountry("CORSICA");
			addrDAO.update(addr);
			Client clt = cltDAO.getClientById(1);
			System.out.print(clt.toString());
			Client clt2 = new Client();
			clt2.setFirstName("noe");
			clt2.setMiddleName("daypi");
			clt2.setLastName("gambini");
			cltDAO.create(clt2);*/
			buildEntities(ds);
			}
		catch (AddressException e) {
			e.printStackTrace();
			}
		catch (ClientException e) {
				e.printStackTrace();
			}
	}
	
	public static void buildEntities(MysqlDataSource ds) throws AddressException, ClientException, ProductException, OrderException, OrderLineException
	{
		AddressDAO ad 		= new AddressDAO(ds);
		ClientDAO cd		= new ClientDAO(ds);
		DvdDAO	dd			= new DvdDAO(ds);
		GameDAO gd			= new GameDAO(ds);
		OrderDAO od			= new OrderDAO(ds);
		OrderLineDAO old	= new OrderLineDAO(ds);
		
		Category c1 = new Category("US");
		//Category c2 = new Category("Asia");
		Category c3 = new Category("Europe");
		
		AbstractProduct p1 = new Dvd(1, "Any Given Sunday", 10);
	/*	AbstractProduct p2 = new Dvd(2, "Braveheart", c3, 15);
		AbstractProduct p3 = new Dvd(3, "Tigres et dragons", c2, 15);
		AbstractProduct p4 = new Dvd(4, "Les grands ducs", c3, 20);*/
		
		AbstractProduct p5 = new Game(5, "Heroes of Might & Magic 6", 50);
		//AbstractProduct p6 = new Game(6, "Civilisation 5", c1, 50);
		
		Address a = new Address();
		
		a.setStreetNumber("42");
		a.setStreetName("King Street");
		a.setCity("Metroplex");
		a.setZipCode("1313");
		a.setCountry("US");
		
		Client c = new Client();
		
		c.setFirstName("Dim");
		c.setMiddleName("Dam");
		c.setLastName("Doum");
		
		Order o = new Order();
		OrderLine ol = new OrderLine(1, 1);
		
		o.setIdClient(1);
		//o.addLine(new OrderLine(1, 1));
		//o.addLine(new OrderLine(2, 2));
		o.setOrderStarted(new GregorianCalendar().getTime());
		
		ad.create(a);
		cd.create(c);
		od.create(o);
		old.create(ol);		
		dd.create((Dvd)p1);
		gd.create((Game)p5);
	}
}
