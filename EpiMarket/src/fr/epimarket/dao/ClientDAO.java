package fr.epimarket.dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import fr.epimarket.annotation.DBTable;
import fr.epimarket.exception.ClientException;

import org.apache.commons.dbutils.DbUtils;

import fr.epimarket.model.Client;

public class	ClientDAO {
	protected DataSource	datasource;
	int rs;
	
	public DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}
	public ClientDAO(DataSource datasource) {
		this.datasource = datasource;
	}

	public ClientDAO() {
	}
	
	public void create(Client client) throws ClientException {
		Connection conn = null;
		PreparedStatement stmt = null;
		String dbName = null;
		String query = null;
		Field[] fields;
		Field field;

		DBTable dbtableAnnotation = Client.class
				.getAnnotation(DBTable.class);
		if (dbtableAnnotation == null) {
			throw new ClientException(new Exception(
					"Entity Should be annotated with @DBTable"));
		}
		dbName = dbtableAnnotation.name();

		try {
			conn = this.datasource.getConnection();
			query = "INSERT INTO " + dbName + " ( ";
			fields = client.getClass().getDeclaredFields();
			for (int i = 1 ; i < fields.length ; i++) { //on commence a un pour skip l'id
				field = fields[i];
				field.setAccessible(true);
				query += field.getName() + (i == fields.length - 1 ? " " : ", ");
			}
			query += " ) VALUES (";
			for (int i = 1 ; i < fields.length ; i++) { //on commence a un pour skip l'id
				field = fields[i];
				field.setAccessible(true);
				query += "'" + field.get(client) + "'" + (i == fields.length - 1 ? " " : ", ");
			}
			query += " );";
			System.out.print("\n ZE QUERI:"+query);
			stmt = conn.prepareStatement(query);
			rs = stmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new ClientException(e);
		} catch (IllegalArgumentException e) {
			throw new ClientException(e);
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(conn);
		}
	}
	
	public void update(Client client) throws ClientException {
		Connection conn = null;
		PreparedStatement stmt = null;
		String dbName = null;
		String query = null;
		Field[] fields;
		Field field;

		DBTable dbtableAnnotation = Client.class
				.getAnnotation(DBTable.class);
		if (dbtableAnnotation == null) {
			throw new ClientException(new Exception(
					"Entity Should be annotated with @DBTable"));
		}
		dbName = dbtableAnnotation.name();

		try {
			conn = this.datasource.getConnection();
			query = "UPDATE " + dbName + " SET ";
			fields = client.getClass().getDeclaredFields();
			for (int i = 0 ; i < fields.length ; i++) {
				field = fields[i];
				field.setAccessible(true);
				query += field.getName() + "='" + field.get(client) + "'" + (i == fields.length - 1 ? " " : ", ");
			}
			query += "WHERE id=?";
			System.out.print(query);
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, client.getId());
			rs = stmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new ClientException(e);
		} catch (IllegalArgumentException e) {
			throw new ClientException(e);
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(conn);
		}
	}
	
	public void	delete(Client client) throws ClientException {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		String dbName = null;

		DBTable dbtableAnnotation = Client.class
				.getAnnotation(DBTable.class);
		if (dbtableAnnotation == null) {
			throw new ClientException(new Exception(
					"Entity Should be annotated with @DBTable"));
		}
		dbName = dbtableAnnotation.name();

		try {
			conn = this.datasource.getConnection();

			stmt = conn.prepareStatement("DELETE * FROM " + dbName
					+ " where id=?");
			stmt.setInt(1, client.getId());
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new ClientException(e);
		} finally {

			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(conn);
		}
	}
	
	public Client getClientById(int Id) throws ClientException {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		String dbName = null;

		DBTable dbtableAnnotation = Client.class
				.getAnnotation(DBTable.class);
		if (dbtableAnnotation == null) {
			throw new ClientException(new Exception(
					"Entity Should be annotated with @DBTable"));
		}
		dbName = dbtableAnnotation.name();

		try {
			conn = this.datasource.getConnection();

			stmt = conn.prepareStatement("SELECT * FROM " + dbName
					+ " where id=?");
			stmt.setInt(1, Id);

			rs = stmt.executeQuery();
			Client address = new Client();
			if (rs.next()) { // at least one record found
				ResultSetMetaData metadata = rs.getMetaData();
				for (int i = 1; i <= metadata.getColumnCount(); i++) {
					String columnName = metadata.getColumnName(i);
					System.out.print(columnName);
					Field field = address.getClass().getDeclaredField(columnName);
					field.setAccessible(true);
					field.set(address, rs.getObject(i));

				}

			}
			return address;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new ClientException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} finally {

			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(conn);
		}
	}
	
	public int getRs() {
		return rs;
	}

	public void setRs(int rs) {
		this.rs = rs;
	}
}
