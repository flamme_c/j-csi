package fr.epimarket.model.state;

import fr.epimarket.model.validation.ValidationException;

public class OrderStateMachine 
{
	public static void changeStep(IStateChangeable statedReference) throws ValidationException
	{
		statedReference.validate();
		
		statedReference.setState(statedReference.getState().getNextStepState());
	}
}
