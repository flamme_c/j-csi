package fr.epimarket.model.state;

import fr.epimarket.model.validation.IValidable;

public interface IStateChangeable extends IValidable
{
	public EOrderState	getState();
	public void 		setState(EOrderState orderState);
}
