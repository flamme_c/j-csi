package fr.epimarket.model.state;

public enum EOrderState 
{
	TRANSACTION_ENDED("Transaction ended", null),
	TRANSACTION_STARTED("Transaction started", TRANSACTION_ENDED),
	TRANSACTION_NULL("Transaction null", TRANSACTION_STARTED);
		
	private EOrderState		nextStepState;
	private String 			state;
		
	private EOrderState(String state, EOrderState nextStepState)
	{
		this.state 			= state;
		this.nextStepState 	= nextStepState;
	}

	public EOrderState getNextStepState() 					{return nextStepState;}
	public String getState() 								{return state;}
}
