package fr.epimarket.model;

public interface IBonusProducer 
{
	public int getBonusPoint();
}
