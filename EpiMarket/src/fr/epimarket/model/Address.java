package fr.epimarket.model;

import fr.epimarket.annotation.DBTable;

@DBTable(name="address")
public class Address 
{
	private int			id;
	private String		streetNumber;
	private String		streetName;
	private String		city;
	private String		zipcode;
	private String		country;
	
	public Address()
	{
		
	}
	
	public String toString()
	{
		return ("Address instance:\nstreetNumber= " + streetNumber + "\nstreetName= " + streetName +  "\ncity= " + city +  "\nzipCode= " + zipcode +  "\ncountry= " + country);
	}

	public String getStreetNumber() 					{return streetNumber;}
	public String getStreetName()						{return streetName;}
	public String getCity()								{return city;}
	public String getZipCode()							{return zipcode;}
	public String getCountry()							{return country;}
	public int	  getId() 								{return id;}

	public void setStreetNumber(String streetNumber) 	{this.streetNumber	= streetNumber;}
	public void setStreetName(String streetName)		{this.streetName	= streetName;}
	public void setCity(String city)					{this.city			= city;}
	public void setZipCode(String zipCode)				{this.zipcode		= zipCode;}
	public void setCountry(String country)				{this.country		= country;}
	public void setId(int id) 							{this.id = id;}
}
