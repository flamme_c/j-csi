package fr.epimarket.model;

import fr.epimarket.annotation.DBTable;

@DBTable(name="product")
public class Game extends AbstractProduct
{
	int					id;
	protected EProduct	idType;
	protected String	name;
	protected int		price;
	
	public Game(int id, String name, int price)
	{
		super(id, name, price, EProduct.GAME);
	}
	
	@Override
	public int getBonusPoint() 
	{
		// TODO Auto-generated method stub
		return (20);
	}
}
