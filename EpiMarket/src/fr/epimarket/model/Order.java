package fr.epimarket.model;

import java.util.Date;

import fr.epimarket.model.state.EOrderState;
import fr.epimarket.model.state.IStateChangeable;
import fr.epimarket.model.validation.ValidationException;
import fr.epimarket.utils.DateHelper;
import fr.epimarket.annotation.DBTable;

@DBTable(name="order")
public class Order implements IStateChangeable
{
	private int					id;
	private int					idClient;
	//private Client				client;
	///// Prendre une liste d'idOrderLine;
	//private List<OrderLine>		lines = new ArrayList<OrderLine>();
	private EOrderState			state;
	private Date				orderStarted;
	private Date				orderEnded;
	
	public Order()
	{
		setState(EOrderState.TRANSACTION_STARTED);
	}
	
	public float getTotal()
	{
		float total = 0;
		/*for (Iterator<OrderLine> i = lines.iterator(); i.hasNext();)
		{
			OrderLine ol = i.next();
			//////////// Faire une fonction qui retourne un product en fonction de son ID
			//total += ol.getQuantity() * ol.getIdProduct().getPrice();
		}*/
		return (total);
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	////////////// Override java.lang.object Method
	/////////////////////////////////////////////////////////////////////////////////////
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("Order: \r\n\t"
				+	((orderStarted != null)? "dateOrderStarted= "	+ DateHelper.getStringDate(orderStarted) : "null"
				+	((orderEnded != null)? "dateOrderEnded= " 		+ DateHelper.getStringDate(orderEnded) : "null")));
		
				sb.append("\r\n");
			//	sb.append("\r\n\tClient: " + ((client != null)? client.toString() : "null"));
				sb.append("\r\n");
				
		/*		for (OrderLine i : lines)
					sb.append("\r\n\t" + i.toString());*/
				
				return sb.toString();
	}
	
	public void validate() throws ValidationException
	{
		if (idClient != 0)
			throw new ValidationException("Order instance : client IS NULL !!");
		
		/*for (Iterator<OrderLine> i = lines.iterator(); i.hasNext();)
		{
			OrderLine ol = i.next();
			
			//if ((ol.getProduct() == null) || (ol.getQuantity() == null))
			//	throw new ValidationException("Order line instance : intance not consistant");
		}*/
	}
	
//public void addLine(OrderLine l)	{lines.add(l);}

	//public Client getClient() 									{return client;}
	//public List<OrderLine> getLines() 							{return lines;}
	public Date getorderStarted() 							{return orderStarted;}
	public Date getorderEnded() 							{return orderEnded;}
	public EOrderState getState() 								{return state;}
	public int getId() 											{return id;}
	public int getIdClient() 									{return idClient;}
	
	//public void setClient(Client client) 						{this.client			= client;}
	//public void setLines(List<OrderLine> lines)					{this.lines 			= lines;}
	public void setOrderStarted(Date dateOrderStarted) 		{this.orderStarted 	= dateOrderStarted;}
	public void setOrderEnded(Date dateOrderEnded) 			{this.orderEnded 	= dateOrderEnded;}
	public void setState(EOrderState state) 					{this.state 			= state;}
	public void setId(int id) 									{this.id = id;}
	public void setIdClient(int id)								{this.idClient = id;}
}
