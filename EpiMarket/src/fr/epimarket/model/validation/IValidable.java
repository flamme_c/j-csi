package fr.epimarket.model.validation;
import fr.epimarket.model.validation.ValidationException;

public interface IValidable 
{
	public void validate() throws ValidationException;
}
