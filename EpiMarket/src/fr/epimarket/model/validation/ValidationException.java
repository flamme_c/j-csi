package fr.epimarket.model.validation;

public class ValidationException extends RuntimeException 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5023252292852234955L;

	public ValidationException(String msg)
	{
		super(msg);
	}
}
