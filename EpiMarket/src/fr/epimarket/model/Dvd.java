package fr.epimarket.model;

import fr.epimarket.annotation.DBTable;

@DBTable(name="product")
public class Dvd extends AbstractProduct
{
	int					id;
	protected EProduct	idType;
	protected String	name;
	protected int		price;

	
	public Dvd(int id, String name, int price)
	{
		super(id, name, price, EProduct.DVD);
	}
}
