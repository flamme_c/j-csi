package fr.epimarket.model;

public abstract class AbstractProduct implements IBonusProducer
{
	int					id;
	protected String	name;
	protected int		price;
	protected EProduct	idType;
	
	public AbstractProduct(int id, String name, int price, EProduct idType)
	{
		createStructure(id, name, price, idType);
	}
	
	public void createStructure(int id, String name, int price, EProduct idType)
	{
		this.id = id;
		this.name = name;
		this.price = price;
		this.idType = idType;
	}
	
	public int getBonusPoint() {return (10);}

	/////////////////////////////////////////////////////////////////////////////////////
	////////////// Getter and setter
	/////////////////////////////////////////////////////////////////////////////////////
	public int getId() 								{return id;}
	public String getname() 						{return name;}
	public EProduct getIdType() 					{return idType;}
	public int getPrice() 							{return price;}

	public void setId(int id)	 					{this.id			= id;}
	public void setname(String name)				{this.name	= name;}
	public void setIdType(EProduct type)			{this.idType	= type;}
	public void setPrice(int price) 				{this.price			= price;}
	
	/////////////////////////////////////////////////////////////////////////////////////
	////////////// Override java.lang.object Method
	/////////////////////////////////////////////////////////////////////////////////////
	public String toString()
	{
		return ("Product instance:\r\n\tid= " + id + "\r\n\tname= " + name + "\r\n\tprice= " + price + "\r\n\ttype= " + idType);
	}
}
