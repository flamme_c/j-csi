package fr.epimarket.model;

import fr.epimarket.annotation.DBTable;

@DBTable(name="client")
public class Client 
{
	private int			id;
	private String		firstName;
	private String		middleName;
	private String		lastName;
	private int			idBillingAddress;
	private int			idDeliverAddress;
	
	public Client()
	{
		
	}
	
	public String toString()
	{
		return ("Client id: " + id + "idBillingAdress: " + idBillingAddress + " idDeliverAddress: " + idDeliverAddress + " " +firstName + " " + middleName + " " + lastName);
	}

	public int getId() 										{return id;}
	public String getFirstName()							{return firstName;}
	public String getMiddleName()							{return middleName;}
	public String getLastName()								{return lastName;}	
	public int getIdDeliverAddress() 						{return idDeliverAddress;}
	public int getIdBillingAdress() 						{return idBillingAddress;}

	
	public void setFirstName(String firstName)				{this.firstName			= firstName;}
	public void setMiddleName(String middleName)			{this.middleName		= middleName;}
	public void setLastName(String lastName)				{this.lastName 			= lastName;}
	public void setIdDeliverAddress(int idDeliverAddress) 	{this.idDeliverAddress 	= idDeliverAddress;}
	public void setIdBillingAdress(int idBillingAdress) 	{this.idBillingAddress 	= idBillingAdress;}
	public void setId(int id) 								{this.id = id;}	
}
