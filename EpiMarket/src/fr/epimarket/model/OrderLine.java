package fr.epimarket.model;

import fr.epimarket.annotation.DBTable;

@DBTable(name="orderline")
public class OrderLine 
{

	int						id;
	int						idProduct;
	//private AbstractProduct	product;
	private Integer			quantity;
	
	public OrderLine()
	{
		
	}
	
	public OrderLine(int idProduct, int quantity)
	{
		setIdProduct(idProduct);
		setQuantity(quantity);
	}
	
	public String toString()
	{
		return (null);
		//return ("OrderLine instance: quantity= " + quantity + "\r\n\t\tassociated Product=" + product.toString());
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	////////////// Getter and setter
	/////////////////////////////////////////////////////////////////////////////////////
	
	//public AbstractProduct getProduct()				{return product;}
	public Integer getQuantity()					{return quantity;}

	//public void setProduct(AbstractProduct product) {this.product	= product;}
	public void setQuantity(Integer quantity)		{this.quantity	= quantity;}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdProduct() {
		return idProduct;
	}
	
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
}
