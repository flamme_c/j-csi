package fr.epimarket.exception;

public class ClientException extends Exception  
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5244455220148984727L;

	public ClientException(Exception e)
	{
		super(e);
	}
}