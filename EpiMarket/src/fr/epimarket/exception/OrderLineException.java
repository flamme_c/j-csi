package fr.epimarket.exception;

public class OrderLineException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7764348213343833515L;

	public OrderLineException(Exception e)
	{
		super(e);
	}
}
