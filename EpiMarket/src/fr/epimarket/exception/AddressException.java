package fr.epimarket.exception;

public class AddressException extends Exception {
	
	private static final long serialVersionUID = 771409590200426358L;
	
	public AddressException(Exception e) {
		super(e);
	}

}