package fr.epimarket.exception;

public class ProductException extends Exception 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5886869224543684044L;

	public ProductException(Exception e)
	{	
		super(e);
	}
}
