package fr.epimarket.exception;

public class OrderException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -446355659140773963L;

	public OrderException(Exception e)
	{
		super(e);
	}
}
