package fr.epimarket.exception;

public class TypeException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4737314292158751916L;

	public TypeException(Exception e)
	{
		super(e);
	}
}
